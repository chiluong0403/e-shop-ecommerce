import React from "react";

const Button = ({ type = "button", className = "", children }) => {
  return (
    <button type={type} className={className}>
      {children}
    </button>
  );
};

export default Button;
