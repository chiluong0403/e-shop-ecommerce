import React, { useEffect, useRef, useState } from "react";
import axios from "axios";
import { useProduct } from "../../context/product-context";
const Cart = () => {
  const [data, setData] = useState([]);
  const { setValue } = useProduct();
  let dataProduct = JSON.parse(localStorage.getItem("dataProduct"));
  let auth = JSON.parse(localStorage.getItem("auth"));
  useEffect(() => {
    axios
      .post("http://localhost/laravel/public/api/product/cart", dataProduct)
      .then(function (response) {
        setData(response.data.data);
      });
  }, []);
  const handleIncrement = (e) => {
    const id = e.target.id;
    if (data) {
      let newData = [...data]; //copy
      newData.map((item, i) => {
        if (item.id === +id) {
          newData[i].qty = newData[i].qty + 1;
        }
        setData(newData);
      });
      let dataLocal = Object.keys(dataProduct).includes(id);
      if (dataLocal) {
        dataProduct = {
          ...dataProduct,
          [id]: dataProduct[id] + 1,
        };
      }
      localStorage.setItem("dataProduct", JSON.stringify(dataProduct));
    }
  };
  const handleDecrement = (e) => {
    let id = e.target.id;
    let newData = [...data];
    newData.map((value, key) => {
      console.log(value);
      if (id == value.id) {
        if (value.qty > 1) {
          return value.qty--;
        } else {
          return newData.splice(key, 1);
        }
      }
    });
    setData(newData);
    Object.keys(dataProduct).map((key, index) => {
      if (id == key) {
        if (dataProduct[key] > 1) {
          return dataProduct[key]--;
        } else {
          delete dataProduct[key];
        }
      }
    });
    localStorage.setItem("dataProduct", JSON.stringify(dataProduct));
  };
  const handleDeleteProduct = (e) => {
    let id = e.currentTarget.id;
    if (data) {
      let newData = [...data]; //copy
      newData.map((item, i) => {
        if (item.id === +id) {
          newData.splice(i, 1);
        }
      });
      setData(newData);
    }
    delete dataProduct[id];
    localStorage.setItem("dataProduct", JSON.stringify(dataProduct));
  };

  function renderCart() {
    if (data.length > 0)
      return data.map((item) => {
        let image = item.image;
        if (image) {
          image = JSON.parse(image);
        }
        return (
          <tr key={item.id}>
            <td className="cart_product">
              <a href>
                <img
                  src={`http://localhost/laravel/public/upload/user/product/${auth.id}/${image[0]}`}
                  alt=""
                  className="w-[80px] h-[80px]"
                />
              </a>
            </td>
            <td className="cart_description">
              <h4>
                <a href>{item.name}</a>
              </h4>
              <p>Web ID: 1089772</p>
            </td>
            <td className="cart_price">
              <p>${item.price}</p>
            </td>
            <td className="cart_quantity">
              <div className="cart_quantity_button">
                <a
                  className="cart_quantity_up cursor-pointer"
                  href
                  onClick={handleIncrement}
                  id={item.id}
                >
                  +
                </a>
                <input
                  className="cart_quantity_input"
                  type="text"
                  name="quantity"
                  value={item.qty}
                  autoComplete="off"
                  size={2}
                  readOnly
                />
                <a
                  className="cart_quantity_down cursor-pointer"
                  href
                  onClick={handleDecrement}
                  id={item.id}
                >
                  -
                </a>
              </div>
            </td>
            <td className="cart_total">
              <p className="cart_total_price">${item.qty * item.price}</p>
            </td>

            <td
              className="cart_delete"
              onClick={handleDeleteProduct}
              id={item.id}
            >
              <a className="cart_quantity_delete" href>
                <i className="fa fa-times" />
              </a>
            </td>
          </tr>
        );
      });
  }
  return (
    <section id="cart_items " className="col-sm-9">
      <div className="container">
        <div className="breadcrumbs">
          <ol className="breadcrumb">
            <li>
              <a href="#">Home</a>
            </li>
            <li className="active">Shopping Cart</li>
          </ol>
        </div>
        <div className="table-responsive cart_info">
          <table className="table table-condensed">
            <thead>
              <tr className="cart_menu">
                <td className="image">Item</td>
                <td className="description" />
                <td className="price">Price</td>
                <td className="quantity">Quantity</td>
                <td className="total">Total</td>
                <td />
              </tr>
            </thead>
            <tbody>{renderCart()}</tbody>
          </table>
        </div>
      </div>
    </section>
  );
};

export default Cart;
