import axios from "axios";
import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

const ProductList = () => {
  const [data, setData] = useState();
  let auth = localStorage.getItem("auth");
  auth = JSON.parse(auth);
  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/product/list")
      .then(function (response) {
        // handle success
        console.log(response);
        setData(response.data.data.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  }, []);
  function renderProduct() {
    if (data) {
      return data.map((item) => {
        const image = JSON.parse(item.image);
        return (
          <div className="col-sm-4" key={item.id}>
            <div className="product-image-wrapper">
              <div className="single-products">
                <div className=" productinfo text-center ">
                  <img
                    src={`http://localhost/laravel/public/upload/user/product/${auth.id}/${image[0]}`}
                    alt=""
                    className="object-cover"
                  />
                  <h2>${item.price}</h2>
                  <p>{item.name}</p>
                  <a href className="btn btn-default add-to-cart">
                    <i className="fa fa-shopping-cart" />
                    Add to cart
                  </a>
                </div>
                <div className="product-overlay">
                  <div className="overlay-content">
                    <h2>${item.price}</h2>
                    <p>{item.name}</p>
                    <a href className="btn btn-default add-to-cart">
                      <i className="fa fa-shopping-cart" />
                      Add to cart
                    </a>
                  </div>
                </div>
              </div>
              <div className="choose">
                <ul className="nav nav-pills nav-justified">
                  <li>
                    <a href="#">
                      <i className="fa fa-plus-square" />
                      Add to wishlist
                    </a>
                  </li>
                  <li>
                    <NavLink to={`/products/detail/${item.id}`}>
                      <i className="fa fa-plus-square" />
                      Product Detail
                    </NavLink>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        );
      });
    }
  }
  return (
    <div className="col-sm-9 padding-right">
      <div className="features_items">
        {/*features_items*/}
        <h2 className="title text-center">Features Items</h2>
        {renderProduct()}
        {renderProduct()}
        {renderProduct()}
        <ul className="pagination">
          <li className="active">
            <a href>1</a>
          </li>
          <li>
            <a href>2</a>
          </li>
          <li>
            <a href>3</a>
          </li>
          <li>
            <a href>»</a>
          </li>
        </ul>
      </div>
      {/*features_items*/}
    </div>
  );
};

export default ProductList;
