import React from "react";

const WrapperForm = ({ className = "col-sm-offset-5 col-sm-4", children }) => {
  return <div className={className}>{children}</div>;
};

export default WrapperForm;
