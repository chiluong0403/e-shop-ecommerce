import React from "react";

const FromCommon = ({ onSubmit, children }) => {
  return (
    <form encType="multipart/form-data" onSubmit={onSubmit}>
      {children}
    </form>
  );
};

export default FromCommon;
