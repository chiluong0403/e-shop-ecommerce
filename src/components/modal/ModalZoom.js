import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function ModalZoom({ src }) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Zoom
      </Button>
      <Modal
        show={show}
        onHide={handleClose}
        animation={false}
        className="bg-[#0000004f] "
      >
        <Button
          variant="secondary"
          onClick={handleClose}
          className=" cursor-pointer  "
        >
          Close
        </Button>
        <Modal.Body>
          <img src={src} alt="" className=" w-full object-cover" />
        </Modal.Body>
      </Modal>
    </>
  );
}
export default ModalZoom;
