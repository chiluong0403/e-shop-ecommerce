import React from "react";

const Select = ({ children }) => {
  return <select>{children}</select>;
};

export default Select;
