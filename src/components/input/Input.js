import React from "react";

const Input = ({ type = "text", name = "", ...props }) => {
  return (
    <input
      type={type}
      name={name}
      defaultValue={""}
      onChange={() => {}}
      {...props}
    />
  );
};

export default Input;
