import React, { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { useProduct } from "../../context/product-context";
const Header = () => {
  const { value } = useProduct();
  const navigate = useNavigate();
  function handleLogout() {
    localStorage.removeItem("isLogin");
    localStorage.removeItem("token");
    localStorage.removeItem("auth");
    navigate("/excu");
  }
  function renderLogin() {
    let logged = localStorage.getItem("isLogin");
    if (logged) {
      return (
        <li>
          <NavLink to={"/excu"} onClick={handleLogout}>
            <i className="fa fa-lock" /> Logout
          </NavLink>
        </li>
      );
    } else {
      return (
        <li>
          <NavLink to={"/excu"}>
            <i className="fa fa-lock" /> Login
          </NavLink>
        </li>
      );
    }
  }

  return (
    <header id="header">
      {/*header*/}
      <div className="header_top">
        {/*header_top*/}
        <div className="container">
          <div className="row">
            <div className="col-sm-6">
              <div className="contactinfo">
                <ul className="nav nav-pills">
                  <li>
                    <a href>
                      <i className="fa fa-phone" /> +2 95 01 88 821
                    </a>
                  </li>
                  <li>
                    <a href>
                      <i className="fa fa-envelope" /> info@domain.com
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="social-icons pull-right">
                <ul className="nav navbar-nav">
                  <li>
                    <a href>
                      <i className="fa fa-facebook" />
                    </a>
                  </li>
                  <li>
                    <a href>
                      <i className="fa fa-twitter" />
                    </a>
                  </li>
                  <li>
                    <a href>
                      <i className="fa fa-linkedin" />
                    </a>
                  </li>
                  <li>
                    <a href>
                      <i className="fa fa-dribbble" />
                    </a>
                  </li>
                  <li>
                    <a href>
                      <i className="fa fa-google-plus" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/*/header_top*/}
      <div className="header-middle">
        {/*header-middle*/}
        <div className="container">
          <div className="row">
            <div className="col-md-4 clearfix">
              <div className="logo pull-left">
                <a href="index.html">
                  <img src="" alt="" />
                </a>
              </div>
              <div className="btn-group pull-right clearfix">
                <div className="btn-group">
                  <button
                    type="button"
                    className="btn btn-default dropdown-toggle usa"
                    data-toggle="dropdown"
                  >
                    USA
                    <span className="caret" />
                  </button>
                  <ul className="dropdown-menu">
                    <li>
                      <a href>Canada</a>
                    </li>
                    <li>
                      <a href>UK</a>
                    </li>
                  </ul>
                </div>
                <div className="btn-group">
                  <button
                    type="button"
                    className="btn btn-default dropdown-toggle usa"
                    data-toggle="dropdown"
                  >
                    DOLLAR
                    <span className="caret" />
                  </button>
                  <ul className="dropdown-menu">
                    <li>
                      <a href>Canadian Dollar</a>
                    </li>
                    <li>
                      <a href>Pound</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-md-8 clearfix">
              <div className="shop-menu clearfix pull-right">
                <ul className="nav navbar-nav">
                  <li>
                    <NavLink to={"/account"}>
                      <i className="fa fa-user" /> Account
                    </NavLink>
                  </li>
                  <li>
                    <a href>
                      <i className="fa fa-star" /> Wishlist
                    </a>
                  </li>
                  <li>
                    <a href="checkout.html">
                      <i className="fa fa-crosshairs" /> Checkout
                    </a>
                  </li>
                  <li className="relative">
                    <NavLink to={"/cart"}>
                      <i className="fa fa-shopping-cart" />
                      <span className="text-red-500">{value}</span>
                    </NavLink>
                  </li>
                  {renderLogin()}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/*/header-middle*/}
      <div className="header-bottom">
        {/*header-bottom*/}
        <div className="container">
          <div className="row">
            <div className="col-sm-9">
              <div className="mainmenu pull-left">
                <ul className="nav navbar-nav  ">
                  <li>
                    <NavLink
                      to={"/"}
                      className={({ isActive }) => (isActive ? "active" : "")}
                    >
                      Home
                    </NavLink>
                  </li>
                  <li className="dropdown">
                    <NavLink
                      to={"/"}
                      className={({ isActive }) => (isActive ? "active" : "")}
                    >
                      <i className="fa fa-angle-down" />
                      Shop
                    </NavLink>

                    <ul role="menu" className="sub-menu">
                      <li>
                        <NavLink to={"/products"}>Products</NavLink>
                      </li>
                      <li>
                        <NavLink to={"/product/details"}>
                          Product Details
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to={"/checkout"}>Checkout</NavLink>
                      </li>
                      <li>
                        <NavLink to={"/cart"}>Cart</NavLink>
                      </li>
                      <li>
                        <a href="login.html">Login</a>
                      </li>
                    </ul>
                  </li>
                  <li className="dropdown">
                    <NavLink
                      to={"/blog/list"}
                      className={({ isActive }) => (isActive ? "active" : "")}
                    >
                      Blog
                      <i className="fa fa-angle-down" />
                    </NavLink>
                    <ul role="menu" className="sub-menu">
                      <li>
                        <NavLink to={"/blog/list"} className="active">
                          Blog List
                          <i className="fa fa-angle-down" />
                        </NavLink>
                      </li>
                      <li>
                        <a href="blog-single.html">Blog Single</a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="404.html">404</a>
                  </li>
                  <li>
                    <a href="contact-us.html">Contact</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-sm-3">
              <div className="search_box pull-right">
                <input type="text" placeholder="Search" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
