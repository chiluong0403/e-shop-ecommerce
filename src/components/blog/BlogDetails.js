import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Comment from "./Comment";
import ListComment from "./ListComment";
import Rating from "./Rating";
const BlogDetails = () => {
  let params = useParams();
  const [data, setData] = useState();
  const [listComment, setListComment] = useState();
  const [listId, setListId] = useState();
  function getCmt(data) {
    if (!data) return;
    const arr = [...listComment];
    const mergeArr = [data, ...arr];
    setListComment(mergeArr);
  }
  function getIdCmt(idCmt) {
    setListId(idCmt);
  }
  useEffect(() => {
    axios
      .get(`http://localhost/laravel/public/api/blog/detail/${params.id}`)
      .then(function (response) {
        if (response) {
          setData(response.data.data);
          setListComment(response.data.data.comment);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function renderBlogDetails() {
    const urlImg = `http://localhost/laravel/public/upload/Blog/image/`;
    if (data) {
      return (
        <div className="single-blog-post">
          <h3>{data.title}</h3>
          <div className="post-meta">
            <ul>
              <li>
                <i className="fa fa-user" /> Mac Doe
              </li>
              <li>
                <i className="fa fa-clock-o" /> 1:33 pm
              </li>
              <li>
                <i className="fa fa-calendar" /> DEC 5, 2013
              </li>
            </ul>
          </div>
          <a href>
            <img src={urlImg + data.image} alt="" />
          </a>
          <p>{data.description}</p> <br />
          <p>{data.description}</p> <br />
          <p>{data.description}</p> <br />
          <p>{data.description}</p> <br />
          <div className="pager-area">
            <ul className="pager pull-right">
              <li>
                <a href="#">Pre</a>
              </li>
              <li>
                <a href="#">Next</a>
              </li>
            </ul>
          </div>
        </div>
      );
    }
  }
  return (
    <>
      <div className="col-sm-9">
        <div className="blog-post-area">
          <h2 className="title text-center">Latest From our Blog</h2>
          <div>{renderBlogDetails()}</div>
        </div>
        {/*/blog-post-area*/}
        <div className="rating-area">
          <Rating params={params}></Rating>
          <ul className="tag">
            <li>TAG:</li>
            <li>
              <a className="color" href>
                Pink <span>/</span>
              </a>
            </li>
            <li>
              <a className="color" href>
                T-Shirt <span>/</span>
              </a>
            </li>
            <li>
              <a className="color" href>
                Girls
              </a>
            </li>
          </ul>
        </div>
        {/*/rating-area*/}
        <div className="socials-share">
          <a href>
            <img src="images/blog/socials.png" alt="" />
          </a>
        </div>
        <ListComment
          listComment={listComment}
          getIdCmt={getIdCmt}
        ></ListComment>
        {/*/Response-area*/}
        <div className="replay-box">
          <div className="row">
            <div className="col-sm-12">
              <h2>Leave a replay</h2>
              <div className="text-area">
                <div className="blank-arrow">
                  <label>Your Name</label>
                </div>
                <span>*</span>
                <Comment
                  listId={listId}
                  params={params}
                  getCmt={getCmt}
                ></Comment>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default BlogDetails;
