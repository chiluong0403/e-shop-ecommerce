import React, { useState } from "react";

const ListComment = ({ listComment, getIdCmt }) => {
  const handleReply = (e) => {
    if (e.target.id) {
      getIdCmt(e.target.id);
    }
  };

  return (
    <div className="response-area">
      <h2> {`${listComment?.length} RESPONSES`}</h2>
      <ul className="media-list">
        {listComment?.length > 0 &&
          listComment.map((item) => (
            <>
              {+item.id_comment === 0 && (
                <li className="media">
                  <a className="pull-left" href="#">
                    <img
                      className="media-object"
                      src="images/blog/man-two.jpg"
                      alt=""
                    />
                  </a>
                  <div className="media-body">
                    <ul className="sinlge-post-meta">
                      <li>
                        <i className="fa fa-user" />
                        {item.name_user}
                      </li>
                      {/* <li>
                        <i className="fa fa-clock-o" /> 1:33 pm
                      </li> */}
                      <li>
                        <i className="fa fa-calendar" /> {item.updated_at}
                      </li>
                    </ul>
                    <p>{item.comment}</p>
                    <label
                      htmlFor="cmt"
                      className="btn btn-primary"
                      onClick={handleReply}
                      id={item.id}
                    >
                      <i className="fa fa-reply" />
                      Replay
                    </label>
                  </div>
                </li>
              )}
              {listComment.length > 0 &&
                listComment.map((data) => (
                  <>
                    {+data.id_comment === +item.id && (
                      <li className="media second-media" key={data.id}>
                        <a className="pull-left" href="#">
                          <img
                            className="media-object"
                            src="images/blog/man-three.jpg"
                            alt=""
                          />
                        </a>
                        <div className="media-body">
                          <ul className="sinlge-post-meta">
                            <li>
                              <i className="fa fa-user" />
                              {data.name_user}
                            </li>
                            {/* <li>
                              <i className="fa fa-clock-o" /> 1:33 pm
                            </li> */}
                            <li>
                              <i className="fa fa-calendar" /> {data.updated_at}
                            </li>
                          </ul>
                          <p>{data.comment}</p>
                          <a className="btn btn-primary" href>
                            <i className="fa fa-reply" />
                            Replay
                          </a>
                        </div>
                      </li>
                    )}
                  </>
                ))}
            </>
          ))}
      </ul>
    </div>
  );
};

export default ListComment;
