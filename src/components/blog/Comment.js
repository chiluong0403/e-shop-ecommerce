import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const Comment = ({ params, getCmt, listId }) => {
  const navigate = useNavigate();
  let isLogin = localStorage.getItem("isLogin");
  const [value, setValue] = useState("");
  const [data, setData] = useState();
  useEffect(() => {
    if (data) {
      getCmt(data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);
  const handleOnChange = (e) => {
    if (!!isLogin === true) {
      setValue(e.target.value);
    } else {
      alert("You are not logged in . Please click Ok to login");
      navigate("/excu");
    }
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let accessToken = localStorage.getItem("token");
    accessToken = JSON.parse(accessToken);
    let auth = localStorage.getItem("auth");
    auth = JSON.parse(auth);
    let url = `http://localhost/laravel/public/api/blog/comment/${params.id}`;
    let config = {
      headers: {
        Authorization: "Bearer " + accessToken,
        "Content-Type": "application/x-www-form-urlencoded",
        Accept: "application/json",
      },
    };
    if (value === "") {
      alert("Please comment article");
    } else {
      const formData = new FormData();
      formData.append("id_blog", params.id);
      formData.append("id_user", auth.id);
      formData.append("id_comment", listId ? listId : 0);
      formData.append("comment", value);
      formData.append("image_user", auth.avatar);
      formData.append("name_user", auth.name);
      axios
        .post(url, formData, config)
        .then(function (response) {
          if (response) {
            console.log(response);
            setData(response.data.data);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    document.getElementById("myForm").reset();
  };
  return (
    <form onSubmit={handleSubmit} id="myForm">
      <textarea
        name="message"
        rows={11}
        defaultValue={""}
        onChange={handleOnChange}
        id="cmt"
      />
      <button type="submit" className="btn btn-primary">
        Post comment
      </button>
    </form>
  );
};

export default Comment;
