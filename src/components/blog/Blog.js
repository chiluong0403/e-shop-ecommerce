import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import axios from "axios";
import MenuLeft from "../main/MenuLeft";

const Blog = () => {
  const [data, setData] = useState();
  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/blog")
      .then(function (response) {
        if (response) {
          setData(response.data.blog.data);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);
  function renderSingleBlog() {
    const urlImg = `http://localhost/laravel/public/upload/Blog/image/`;
    if (data?.length > 0) {
      return data.map((item) => (
        <div className="single-blog-post" key={item.id}>
          <h3>{item.title}</h3>
          <div className="post-meta">
            <ul>
              <li>
                <i className="fa fa-user" /> Mac Doe
              </li>
              <li>
                <i className="fa fa-clock-o" /> 1:33 pm
              </li>
              <li>
                <i className="fa fa-calendar" /> DEC 5, 2013
              </li>
            </ul>
            <span>
              <i className="fa fa-star" />
              <i className="fa fa-star" />
              <i className="fa fa-star" />
              <i className="fa fa-star" />
              <i className="fa fa-star-half-o" />
            </span>
          </div>
          <NavLink to={`/blog/detail/${item.id}`}>
            <img src={urlImg + item.image} alt="" />
          </NavLink>
          <p>{item.description}</p>
          <NavLink to={`/blog/detail/${item.id}`} className="btn btn-primary">
            Read More
          </NavLink>
        </div>
      ));
    }
  }
  return (
    <>
      <div className="col-sm-9">
        <div className="blog-post-area">
          <h2 className="title text-center">Latest From our Blog</h2>
          <div>{renderSingleBlog()}</div>
          <div className="pagination-area">
            <ul className="pagination">
              <li>
                <a href className="active">
                  1
                </a>
              </li>
              <li>
                <a href>2</a>
              </li>
              <li>
                <a href>3</a>
              </li>
              <li>
                <a href>
                  <i className="fa fa-angle-double-right" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default Blog;
