import React, { useEffect, useState } from "react";
import StarRatings from "react-star-ratings";
import { useNavigate } from "react-router-dom";
import { FaStar } from "react-icons/fa";
import axios from "axios";
const Rating = ({ params }) => {
  const [rating, setRating] = useState(0);
  const [totalRate, setTotalRate] = useState([]);
  let isLogin = localStorage.getItem("isLogin");
  const navigate = useNavigate();
  function changeRating(newRating, name) {
    let accessToken = localStorage.getItem("token");
    accessToken = JSON.parse(accessToken);
    let auth = localStorage.getItem("auth");
    auth = JSON.parse(auth);
    let url = `http://localhost/laravel/public/api/blog/rate/${params.id}`;
    let config = {
      headers: {
        Authorization: "Bearer " + accessToken,
        "Content-Type": "application/x-www-form-urlencoded",
        Accept: "application/json",
      },
    };
    if (!isLogin) navigate("/excu");
    setRating(newRating);
    const formData = new FormData();
    formData.append("blog_id", params.id);
    formData.append("user_id", auth.id);
    formData.append("rate", rating);
    axios
      .post(url, formData, config)
      .then(function (response) {
        if (response) {
          console.log(response);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  useEffect(() => {
    axios
      .get(`http://localhost/laravel/public/api/blog/rate/${params.id}`)
      .then(function (response) {
        if (response) {
          setTotalRate(response.data.data);
          // setTotalRate(() => response.data.data.map((item) => item.rate));
          // response.data.data.map((item, index) => rateData.push(item.rate));
          const data = response.data.data;
          const lengthRate = data.length;
          let amountRate = 0;
          data.map((item) => {
            if (item) {
              amountRate += item.rate;
            }
          });
          setRating(amountRate / lengthRate);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);
  return (
    <ul className="ratings">
      <li className="rate-this">
        Rate this item: {parseFloat(rating).toFixed(1)}{" "}
        <FaStar size={10} color="#FE980F" />
      </li>
      <StarRatings
        rating={rating}
        starRatedColor={"#FE980F"}
        starEmptyColor="#CCCCCC"
        starHoverColor="#FE980F"
        changeRating={changeRating}
        numberOfStars={5}
        name="rating"
      />

      <li className="color">{`${totalRate.length} votes`}</li>
    </ul>
  );
};

export default Rating;
