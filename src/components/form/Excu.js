import React from "react";
import Login from "./Login";
import Register from "./Register";

const Excu = () => {
  return (
    <section id="form">
      <Login></Login>
      <div className="col-sm-1 col-sm-offset-1">
        <h2 className="or">OR</h2>
      </div>
      <Register></Register>
    </section>
  );
};

export default Excu;
