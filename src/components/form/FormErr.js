import React from "react";

const FormErr = ({ err }) => {
  function renderErr() {
    if (Object.values(err).length > 0) {
      return Object.keys(err).map((key, index) => (
        <li key={index}>{err[key]}</li>
      ));
    }
  }
  return <div className="errForm">{renderErr()}</div>;
};

export default FormErr;
