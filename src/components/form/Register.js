import React, { useState } from "react";
import FormErr from "./FormErr";
import axios from "axios";
const Register = ({ heading, className = "" }) => {
  const [err, setErr] = useState({});
  const [infoFile, setInfoFile] = useState();
  const [avatar, setAvatar] = useState();
  const [value, setValue] = useState({
    name: "",
    email: "",
    password: "",
    phone: "",
    avatar,
    level: "0",
  });
  const handleOnChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setValue((preValue) => ({ ...preValue, [name]: value }));
  };
  const handleFile = (e) => {
    const file = e.target.files;
    let render = new FileReader();
    render.onload = (e) => {
      setAvatar(e.target.result);
      setInfoFile(file[0]);
    };
    render.readAsDataURL(file[0]);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    let allErr = {};
    let x = true;
    if (value.name === "") {
      allErr.name = "Please select your name";
      x = false;
    }
    if (value.email === "") {
      allErr.email = "Please enter your email";
      x = false;
    } else {
      if (!regex.test(value.email)) {
        allErr.formatErr =
          "Please enter the correct email . Example:abc@gmail.com";
        x = false;
      }
    }
    if (value.password === "") {
      allErr.pass = "Please enter your pass";
      x = false;
    }
    if (value.phone === "") {
      allErr.phone = "Please enter your phone number";
      x = false;
    }
    if (value.avatar === "") {
      allErr.avatar = "Please upload file";
      x = false;
    } else {
      if (infoFile) {
        for (let i = 0; i < infoFile.length; i++) {
          const type = infoFile[i]["type"].slice(6);
          const size = infoFile[i]["size"];
          if (size > 1024 * 1024) {
            allErr.large = "File over 1mb";
            x = false;
          }
          if (type !== "jpeg" && type !== "jpg" && type !== "png") {
            allErr.errFormat = "This is error Format";
            x = false;
          }
        }
      }
    }
    if (avatar) {
      value["avatar"] = avatar;
    }
    if (!x) {
      setErr(allErr);
    } else {
      setErr({});
      axios
        .post("http://localhost/laravel/public/api/register", value)
        .then(function (response) {
          if (response.data.errors) {
            setErr(response.data.errors);
          } else {
            alert("Register successfully");
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };
  return (
    <div className="col-sm-offset-1 col-sm-4">
      <div className="signup-form">
        <h2>{heading || "New User Signup!"}</h2>
        <form onSubmit={handleSubmit} encType="multipart/form-data">
          <input
            type="text"
            placeholder="Name"
            name="name"
            onChange={handleOnChange}
          />
          <input
            type="email"
            placeholder="Email"
            name="email"
            onChange={handleOnChange}
          />
          <input
            type="password"
            placeholder="Password"
            name="password"
            onChange={handleOnChange}
          />
          <input
            type="text"
            placeholder="Phone"
            name="phone"
            onChange={handleOnChange}
          />
          <input
            type="text"
            placeholder="Address"
            name="address"
            onChange={handleOnChange}
          />
          <input
            type="file"
            placeholder="Avatar"
            name="avatar"
            onChange={handleFile}
          />
          <input
            type="type"
            placeholder="Level"
            name="level"
            value={"0"}
            onChange={handleOnChange}
          />
          <button type="submit" className="btn btn-default">
            Signup
          </button>
          <FormErr err={err}></FormErr>
        </form>
      </div>
      {/*/sign up form*/}
    </div>
  );
};

export default Register;
