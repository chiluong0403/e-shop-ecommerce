import axios from "axios";
import React, { useState } from "react";
import FormErr from "./FormErr";
import { useNavigate } from "react-router-dom";
const Login = () => {
  const navigate = useNavigate();
  const [err, setErr] = useState({});
  const [value, setValue] = useState({
    email: "",
    password: "",
    level: 0,
  });
  const handleSubmit = (e) => {
    e.preventDefault();
    let allErr = {};
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    let x = true;
    e.preventDefault();
    if (value.email === "") {
      allErr.email = "Please enter your email";
      x = false;
    } else {
      if (!regex.test(value.email)) {
        allErr.formatErr =
          "Please enter the correct email . Example:abc@gmail.com";
        x = false;
      }
    }
    if (value.password === "") {
      allErr.pass = "Please enter your pass";
      x = false;
    }
    if (!x) {
      setErr(allErr);
    } else {
      setErr({});
      axios
        .post("http://localhost/laravel/public/api/login", value)
        .then(function (response) {
          if (response.data.errors) {
            setErr(response.data.errors);
          } else {
            alert("Login Successfully");
            navigate("/");
            let token = response.data.success.token;
            let isLogin = true;
            let auth = response.data.Auth;

            localStorage.setItem("isLogin", JSON.stringify(isLogin));
            localStorage.setItem("token", JSON.stringify(token));
            localStorage.setItem("auth", JSON.stringify(auth));
            setErr("");
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };
  const handleOnChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setValue((preValue) => ({ ...preValue, [name]: value }));
  };
  return (
    <div className="col-sm-4  col-sm-offset-1">
      <div className="login-form">
        <h2>Login to your account</h2>
        <form onSubmit={handleSubmit}>
          <input
            name="email"
            type="email"
            placeholder="Email Address"
            onChange={handleOnChange}
          />
          <input
            name="password"
            type="password"
            placeholder="Password"
            onChange={handleOnChange}
          />
          {/* <span>
            <input type="checkbox" className="checkbox" />
            Keep me signed in
          </span> */}
          <button type="submit" className="btn btn-default">
            Login
          </button>
          <FormErr err={err}></FormErr>
        </form>
      </div>
    </div>
  );
};

export default Login;
