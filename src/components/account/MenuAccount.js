import React from "react";
import { NavLink } from "react-router-dom";

const MenuAccount = () => {
  return (
    <>
      <div className="col-sm-3">
        <div className="left-sidebar">
          <h2>Account</h2>
          <div className="panel-group category-products" id="accordian">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h4 className="panel-title">
                  <NavLink to={"/account"}>
                    <span className="badge pull-right">
                      <i className="fa fa-plus" />
                    </span>
                    ACCOUNT
                  </NavLink>
                </h4>
              </div>
            </div>
            <div className="panel panel-default">
              <div className="panel-heading">
                <h4 className="panel-title">
                  <NavLink to={"/account/add-product"}>
                    <span className="badge pull-right">
                      <i className="fa fa-plus" />
                    </span>
                    Create Product
                  </NavLink>
                </h4>
              </div>
            </div>
            <div className="panel panel-default">
              <div className="panel-heading">
                <h4 className="panel-title">
                  <NavLink to={"/account/my-product"}>
                    <span className="badge pull-right">
                      <i className="fa fa-plus" />
                    </span>
                    My Product
                  </NavLink>
                </h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MenuAccount;
