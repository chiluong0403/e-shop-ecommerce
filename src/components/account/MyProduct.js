import React, { useEffect, useState } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";
const MyProduct = () => {
  const [data, setData] = useState([]);
  let accessToken = localStorage.getItem("token");
  accessToken = JSON.parse(accessToken);
  let auth = localStorage.getItem("auth");
  auth = JSON.parse(auth);
  let config = {
    headers: {
      Authorization: "Bearer " + accessToken,
      "Content-Type": "application/x-www-form-urlencoded",
      Accept: "application/json",
    },
  };
  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/user/my-product", config)
      .then(function (response) {
        const responseData = response.data.data;
        setData(responseData);
      })
      .catch(function (error) {
        console.log(error);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  function deleteProduct(e) {
    let id = e.target.parentElement.id;
    console.log("deleteProduct ~ id", id);
    axios
      .get(
        "http://localhost/laravel/public/api/user/delete-product/" + id,
        config
      )
      .then((res) => {
        setData(res.data.data);
      });
  }
  function handleMyProduct() {
    if (data) {
      return Object.keys(data).map((key, index) => {
        let img = JSON.parse(data[key].image);
        return (
          <tr key={index}>
            <td className="text-xl font-medium">{data[key].id}</td>
            <td className="text-xl  font-medium">
              {data[key].name.toUpperCase()}
            </td>
            <td>
              <img
                src={
                  "http://localhost/laravel/public/upload/user/product/" +
                  auth.id +
                  "/" +
                  img[0]
                }
                className="w-[50px] h-[50px] object-cover"
                alt=""
              />
            </td>
            <td>${data[key].price}</td>
            <td className="w-[50px] ">
              <NavLink to={"/account/edit-product/" + data[key].id}>
                <span className="">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-8 h-8 mx-auto"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                    />
                  </svg>
                </span>
              </NavLink>
            </td>
            <td className="w-[50px]">
              <a id={data[key].id} onClick={deleteProduct} href>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-8 h-8 mx-auto cursor-pointer"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                  />
                </svg>
              </a>
            </td>
          </tr>
        );
      });
    }
  }
  return (
    <div className=" col-sm-9">
      <div className="table-responsive cart_info">
        <table className="table table-condensed">
          <thead>
            <tr className="cart_menu">
              <td className="image">ID</td>
              <td className="description">Name</td>
              <td className="price">Image</td>
              <td className="quantity">Price</td>
              <td className="total">Action</td>
              <td></td>
            </tr>
          </thead>
          <tbody className="">{handleMyProduct()}</tbody>
        </table>
      </div>
    </div>
  );
};

export default MyProduct;
