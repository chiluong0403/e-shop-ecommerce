import React, { useEffect, useState } from "react";
import axios from "axios";
import Button from "../button/Button";
import FormErr from "../form/FormErr";
import FromCommon from "../formCommon/FromCommon";
import WrapperForm from "../formCommon/WrapperForm";
import Heading from "../heading/Heading";
import Input from "../input/Input";
const AddProduct = () => {
  const [brand, setBrand] = useState([]);
  const [category, setCategory] = useState([]);
  const [value, setValue] = useState({
    name: "",
    price: "",
    brand: "",
    category: "",
    status: "",
    company: "",
    detail: "",
    sale: 0,
  });
  const [file, setFile] = useState("");
  const [err, setErr] = useState({});
  let accessToken = localStorage.getItem("token");
  accessToken = JSON.parse(accessToken);
  let config = {
    headers: {
      Authorization: "Bearer " + accessToken,
      "Content-Type": "application/x-www-form-urlencoded",
      Accept: "application/json",
    },
  };
  function handleCheckFile(errorSubmit, flag) {
    const arrType = ["png", "jpg", "jpeg", "PNG", "JPG"];
    if (file.length <= 3) {
      Object.keys(file).map((key, index) => {
        let nameFile = file[key].name;
        let typeFile = nameFile.split(".")[1];
        console.log(typeFile);
        if (file[key].size > 1024 * 1024) {
          errorSubmit.file = "file: kich thuoc lon";
          flag = false;
        }
        if (!arrType.includes(typeFile)) {
          errorSubmit.file = "file: khong dung";
          flag = false;
        }
      });
    } else {
      errorSubmit.file = "file: qua so luong cho phep";
      flag = false;
    }
  }
  const handleSubmit = (e) => {
    e.preventDefault();
    let allErr = {};
    let x = true;
    if (value.name === "") {
      allErr.name = "Please enter your name product";
      x = false;
    }
    if (value.price === "") {
      allErr.price = "Please enter your price product";
      x = false;
    }
    if (value.brand === "") {
      allErr.brand = "Please select your name product";
      x = false;
    }
    if (value.category === "") {
      allErr.category = "Please select your name category";
      x = false;
    }
    if (value.status === "") {
      allErr.status = "Please select your status";
      x = false;
    } else {
      if (value.sale === "" && value.status === "sale") {
        allErr.sale = "Sale k dc de trong";
        x = false;
      }
    }
    if (value.company === "") {
      allErr.company = "Please enter your info company";
      x = false;
    }
    if (value.detail === "") {
      allErr.detail = "Please enter detail product";
      x = false;
    }
    if (file === "") {
      allErr.file = "file : khong dc trong";
      x = false;
    } else {
      handleCheckFile(allErr, x);
    }

    if (!x) {
      setErr(allErr);
    } else {
      setErr("");
      value["file"] = file;
      const formData = new FormData();
      formData.append("name", value.name);
      formData.append("price", value.price);
      formData.append("category", value.category);
      formData.append("brand", value.brand);
      formData.append("company", value.company);
      formData.append("detail", value.detail);
      formData.append("status", value.status);
      formData.append("sale", value.sale ? value.sale : "");
      Object.keys(file).map((key, index) => {
        formData.append("file[]", file[key]);
      });
      axios
        .post(
          "http://localhost/laravel/public/api/user/add-product",
          formData,
          config
        )
        .then((response) => {
          console.log(response);
          if (response.data.errors) {
            setErr(response.data.errors);
          } else {
            setErr("");
          }
        });
    }
  };
  const handleOnChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setValue((preValue) => ({ ...preValue, [name]: value }));
  };
  const handleFile = (e) => {
    const file = e.target.files;
    setFile(file);
  };
  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/category-brand")
      .then(function (response) {
        if (response) {
          setBrand(response.data.brand);
          setCategory(response.data.category);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);
  return (
    <WrapperForm>
      <div className="add-product">
        <Heading>Add Product</Heading>
        <FromCommon onSubmit={handleSubmit}>
          <Input
            type="text"
            name="name"
            onChange={handleOnChange}
            placeholder="Name"
          ></Input>
          <Input
            name="price"
            onChange={handleOnChange}
            placeholder="Price"
            type="number"
          ></Input>
          <select name="brand" onChange={handleOnChange}>
            <option value="">Please choose brand</option>
            {brand.length > 0 &&
              brand.map((item) => (
                <option value={item.id} key={item.id}>
                  {item.brand}{" "}
                </option>
              ))}
          </select>
          <select name="category" onChange={handleOnChange}>
            <option value="">Please choose category</option>
            {category.length > 0 &&
              category.map((item) => (
                <option value={item.id} key={item.id}>
                  {item.category}
                </option>
              ))}
          </select>
          <select name="status" onChange={handleOnChange}>
            <option value="">Status</option>
            <option value="0">Sale</option>
            <option value="1">New</option>
          </select>
          {value.status === "0" && (
            <Input
              name="sale"
              onChange={handleOnChange}
              placeholder="Sale"
            ></Input>
          )}
          <Input
            name="company"
            onChange={handleOnChange}
            placeholder="Company profile"
            type="text"
          ></Input>
          <Input
            type="file"
            placeholder="Avatar"
            name="file"
            multiple
            onChange={handleFile}
          ></Input>
          <textarea
            name="detail"
            cols="30"
            rows="10"
            placeholder="Details"
            onChange={handleOnChange}
          ></textarea>
          <Button type="submit" className="btn btn-default">
            Add Product
          </Button>
          <FormErr err={err}></FormErr>
        </FromCommon>
      </div>
    </WrapperForm>
  );
};

export default AddProduct;
