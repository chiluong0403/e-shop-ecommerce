import React, { useEffect, useState } from "react";
import axios from "axios";
import $ from "jquery";
import Button from "../button/Button";
import FormErr from "../form/FormErr";
import FromCommon from "../formCommon/FromCommon";
import WrapperForm from "../formCommon/WrapperForm";
import Heading from "../heading/Heading";
import Input from "../input/Input";
import { useParams } from "react-router-dom";
const EditProduct = () => {
  const params = useParams();
  const [data, setData] = useState([]);
  const [file, setFile] = useState("");
  const [brand, setBrand] = useState([]);
  const [category, setCategory] = useState([]);
  const [err, setErr] = useState({});
  const [avatarCheckbox, setAvatarCheckBox] = useState([]);
  const image = data.image;
  let accessToken = localStorage.getItem("token");
  accessToken = JSON.parse(accessToken);
  let auth = localStorage.getItem("auth");
  auth = JSON.parse(auth);
  let config = {
    headers: {
      Authorization: "Bearer " + accessToken,
      "Content-Type": "application/x-www-form-urlencoded",
      Accept: "application/json",
    },
  };
  const handleSubmit = (e) => {
    console.log(avatarCheckbox);
    e.preventDefault();
    let allErr = {};
    let x = true;
    if (file.length > 3) {
      allErr.file = "file: qua so luong cho phep";
      x = false;
    }
    if (!x) {
      setErr(allErr);
    } else {
      setErr("");
      const formData = new FormData();
      formData.append("category", data.id_category);
      formData.append("brand", data.id_brand);
      formData.append("name", data.name);
      formData.append("detail", data.detail);
      formData.append("company", data.company_profile);
      formData.append("price", data.price);
      Object.keys(file).map((key, index) => {
        formData.append("file[]", file[key]);
      });
      avatarCheckbox.map((item) => {
        console.log(item);
        formData.append("avatarCheckBox[]", item);
      });
      axios
        .post(
          `http://localhost/laravel/public/api/user/edit-product/${params.id}`,
          formData,
          config
        )
        .then(function (response) {
          // handle success
          console.log(response);
          if (response.data.errors) {
            setErr(response.data.errors);
          }
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    }
  };
  const handleOnChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setData((preValue) => ({ ...preValue, [name]: value }));
  };
  const handleFile = (e) => {
    const file = e.target.files;
    setFile(file);
  };

  const handleCheckbox = (e) => {
    let getValue = e.target.value;
    if (e.target.checked) {
      setAvatarCheckBox((state) => [...state, getValue]);
    } else {
      let result = avatarCheckbox.filter(function (elem) {
        return elem !== getValue;
      });
      setAvatarCheckBox(result);
      setFile(result);
    }
  };
  useEffect(() => {
    axios
      .get(
        `http://localhost/laravel/public/api/user/product/${params.id}`,
        config
      )
      .then(function (response) {
        setData(response.data.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  }, []);
  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/category-brand")
      .then(function (response) {
        if (response) {
          setBrand(response.data.brand);
          setCategory(response.data.category);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);
  return (
    <WrapperForm>
      <div className="add-product">
        <Heading>Edit Product</Heading>
        <FromCommon onSubmit={handleSubmit}>
          <Input
            type="text"
            name="name"
            onChange={handleOnChange}
            placeholder="Name"
            defaultValue={data.name}
          ></Input>
          <Input
            name="price"
            onChange={handleOnChange}
            placeholder="Price"
            type="number"
            defaultValue={data.price}
          ></Input>
          <select name="id_brand" onChange={handleOnChange}>
            <option value="">Please choose brand</option>
            {brand.map((item) => {
              return (
                <>
                  <option
                    value={item.id === data.id_brand ? data.id_brand : item.id}
                    selected={item.id === data.id_brand}
                  >
                    {item.brand}
                  </option>
                </>
              );
            })}
          </select>
          <select name="category" onChange={handleOnChange}>
            <option value="">Please choose category</option>
            {category.map((item) => {
              return (
                <option
                  value={
                    item.id === data.id_category ? data.id_category : item.id
                  }
                  selected={item.id === data.id_category}
                >
                  {item.category}
                </option>
              );
            })}
          </select>
          <select name="status" onChange={handleOnChange}>
            <option value="">Status</option>
            <option value="1" selected={data.status === 1}>
              New
            </option>
            <option value="0" selected={data.status === 0}>
              Sale
            </option>
          </select>
          {data.status == 0 && (
            <Input
              name="sale"
              onChange={handleOnChange}
              placeholder="Sale"
              defaultValue={data.sale}
            ></Input>
          )}
          <Input
            name="company"
            onChange={handleOnChange}
            placeholder="Company profile"
            type="text"
            defaultValue={data.company_profile}
          ></Input>
          <div>
            <Input
              type="file"
              placeholder="Avatar"
              name="file"
              multiple
              onChange={handleFile}
            ></Input>
            <div className="flex items-center justify-between">
              {image &&
                image.map((item) => {
                  return (
                    <div className="flex justify-center flex-col items-center">
                      <img
                        src={`http://localhost/laravel/public/upload/user/product/${auth.id}/${item}`}
                        alt=""
                        className="w-[50px] h-[50px]"
                      />
                      <input
                        value={item}
                        type="checkbox"
                        onChange={handleCheckbox}
                        style={{ width: 20, height: 20 }}
                      />
                    </div>
                  );
                })}
            </div>
          </div>
          <textarea
            rows={10}
            name="detail"
            placeholder="Details"
            onChange={handleOnChange}
            defaultValue={data.detail}
            className="p-3 focus:outline-blue-500 transition-all"
          ></textarea>
          <Button type="submit" className="btn btn-default">
            Add Product
          </Button>
          <FormErr err={err}></FormErr>
        </FromCommon>
      </div>
    </WrapperForm>
  );
};

export default EditProduct;
