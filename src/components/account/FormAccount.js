import axios from "axios";
import React, { useState } from "react";
import Button from "../button/Button";
import FromCommon from "../formCommon/FromCommon";
import WrapperForm from "../formCommon/WrapperForm";
import Heading from "../heading/Heading";
import Input from "../input/Input";

const FormAccount = () => {
  let auth = localStorage.getItem("auth");
  auth = JSON.parse(auth);
  let accessToken = localStorage.getItem("token");
  accessToken = JSON.parse(accessToken);
  const [infoFile, setInfoFile] = useState();
  const [avatar, setAvatar] = useState();
  const [value, setValue] = useState({
    name: auth.name,
    email: auth.email,
    password: "",
    phone: auth.phone,
    avatar: "",
    level: "0",
    address: auth.address,
  });
  let url = `http://localhost/laravel/public/api/user/update/${auth.id}`;
  let config = {
    headers: {
      Authorization: "Bearer " + accessToken,
      "Content-Type": "application/x-www-form-urlencoded",
      Accept: "application/json",
    },
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (avatar) {
      value["avatar"] = avatar;
    }
    axios
      .post(url, value, config)
      .then(function (response) {
        console.log(response);
        let token = response.data.success.token;
        let isLogin = true;
        let auth = response.data.Auth;
        if (accessToken === token) {
          console.log(123);
        }
        localStorage.setItem("isLogin", JSON.stringify(isLogin));
        localStorage.setItem("token", JSON.stringify(token));
        localStorage.setItem("auth", JSON.stringify(auth));
        // alert("Update profile successfully");
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const handleOnChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setValue((preValue) => ({ ...preValue, [name]: value }));
  };
  const handleFile = (e) => {
    const file = e.target.files;
    let render = new FileReader();
    render.onload = (e) => {
      setAvatar(e.target.result);
      setInfoFile(file[0]);
    };
    render.readAsDataURL(file[0]);
  };

  return (
    <WrapperForm>
      <div className="update-form">
        <Heading>Update</Heading>
        <FromCommon onSubmit={handleSubmit}>
          <Input
            type="text"
            name="name"
            onChange={handleOnChange}
            defaultValue={auth.name}
            placeholder="Name"
          ></Input>
          <Input
            name="email"
            onChange={handleOnChange}
            placeholder="Email"
            defaultValue={auth.email}
            readOnly
            type="email"
          ></Input>
          <Input
            name="password"
            onChange={handleOnChange}
            placeholder="Password"
            type="password"
          ></Input>
          <Input
            name="phone"
            onChange={handleOnChange}
            placeholder="Phone"
            type="text"
            defaultValue={auth.phone}
          ></Input>
          <Input
            name="address"
            onChange={handleOnChange}
            placeholder="Address"
            type="text"
            defaultValue={auth.address}
          ></Input>
          <Input
            type="file"
            placeholder="Avatar"
            name="avatar"
            onChange={handleFile}
          ></Input>
          <Input
            type="number"
            placeholder="Level"
            name="level"
            defaultValue="0"
          ></Input>
          <Button type="submit" className="btn btn-default">
            Update Info
          </Button>
        </FromCommon>
      </div>
    </WrapperForm>
  );
};

export default FormAccount;
