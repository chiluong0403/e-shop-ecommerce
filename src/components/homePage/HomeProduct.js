import axios from "axios";
import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { useProduct } from "../../context/product-context";
const HomeProduct = () => {
  const [data, setData] = useState();
  let auth = JSON.parse(localStorage.getItem("auth"));
  const [objProduct, setObjProduct] = useState({});
  let dataProduct = JSON.parse(localStorage.getItem("dataProduct"));
  const { setValue } = useProduct();
  useEffect(() => {
    function totalQty() {
      let sum = 0;
      Object.keys(objProduct).map((key) => {
        sum += objProduct[key];
        return setValue(sum);
      });
    }
    totalQty();
  }, [objProduct, setValue]);
  const handleAddCart = (e) => {
    const id = e.target.id;
    let a = Object.keys(objProduct).includes(id);
    if (a) {
      setObjProduct((state) => ({ ...state, [id]: objProduct[id] + 1 }));
      console.log(123);
    } else {
      console.log("handleAddCart ~ objProduct", objProduct);
      setObjProduct((state) => ({ ...state, [id]: 1 }));
      console.log("abc");
    }
    localStorage.setItem("dataProduct", JSON.stringify(objProduct));
  };
  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/product")
      .then(function (response) {
        // handle success
        setData(response.data.data);
      });
    if (dataProduct) {
      setObjProduct({ ...objProduct, ...dataProduct });
    }
  }, []);
  function renderProduct() {
    if (data) {
      return data.map((item) => {
        const image = JSON.parse(item.image);
        return (
          <div className="col-sm-4" key={item.id}>
            <div className="product-image-wrapper">
              <div className="single-products">
                <div className=" productinfo text-center ">
                  <img
                    src={`http://localhost/laravel/public/upload/user/product/${auth.id}/${image[0]}`}
                    alt=""
                    className="object-cover"
                  />
                  <h2>${item.price}</h2>
                  <p>{item.name}</p>
                  <a href className="btn btn-default add-to-cart">
                    <i className="fa fa-shopping-cart" />
                    Add to cart
                  </a>
                </div>
                <div className="product-overlay">
                  <div className="overlay-content">
                    <h2>${item.price}</h2>
                    <p>{item.name}</p>
                    <a
                      href
                      className="btn btn-default add-to-cart"
                      onClick={handleAddCart}
                      id={item.id}
                    >
                      <i className="fa fa-shopping-cart" />
                      Add to cart
                    </a>
                  </div>
                </div>
              </div>
              <div className="choose">
                <ul className="nav nav-pills nav-justified">
                  <li>
                    <a href="#">
                      <i className="fa fa-plus-square" />
                      Add to wishlist
                    </a>
                  </li>
                  <li>
                    <NavLink to={`/products/detail/${item.id}`}>
                      <i className="fa fa-plus-square" />
                      Product Detail
                    </NavLink>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        );
      });
    }
  }

  return (
    <>
      <div className="col-sm-9 padding-right">
        <div className="features_items">
          {/*features_items*/}
          <h2 className="title text-center">Features Items</h2>
          {renderProduct()}
        </div>
        {/*features_items*/}
        <div className="category-tab">
          {/*category-tab*/}
          <div className="col-sm-12">
            <ul className="nav nav-tabs">
              <li className="active">
                <a href="#tshirt" data-toggle="tab">
                  T-Shirt
                </a>
              </li>
              <li>
                <a href="#blazers" data-toggle="tab">
                  Blazers
                </a>
              </li>
              <li>
                <a href="#sunglass" data-toggle="tab">
                  Sunglass
                </a>
              </li>
              <li>
                <a href="#kids" data-toggle="tab">
                  Kids
                </a>
              </li>
              <li>
                <a href="#poloshirt" data-toggle="tab">
                  Polo shirt
                </a>
              </li>
            </ul>
          </div>
          <div className="tab-content">
            <div className="tab-pane fade active in" id="tshirt">
              {renderProduct()}
            </div>
          </div>
        </div>
        {/*/category-tab*/}
        <div className="recommended_items">
          {/*recommended_items*/}
          <h2 className="title text-center">Recommended items</h2>
          {renderProduct()}
        </div>
        {/*/recommended_items*/}
      </div>
    </>
  );
};

export default HomeProduct;
