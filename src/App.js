import { useLocation } from "react-router-dom";
import MenuAccount from "./components/account/MenuAccount";
import Footer from "./components/main/Footer";
import Header from "./components/main/Header";
import MenuLeft from "./components/main/MenuLeft";
import { ProductProvider } from "./context/product-context";

function App({ children }) {
  let params = useLocation();

  return (
    <>
      <ProductProvider>
        <Header />
        <section>
          <div className="container">
            <div className="row">
              {params["pathname"].includes("account") ? (
                <MenuAccount />
              ) : (
                <MenuLeft />
              )}
              {children}
            </div>
          </div>
        </section>
        <Footer />
      </ProductProvider>
    </>
  );
}
export default App;
