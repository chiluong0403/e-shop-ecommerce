import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Account from "./components/account/Account";
import Excu from "./components/form/Excu";
import BlogDetails from "./components/blog/BlogDetails";
import Blog from "./components/blog/Blog";
import Home from "./components/homePage/Home";
import AddProduct from "./components/account/AddProduct";
import MyProduct from "./components/account/MyProduct";
import EditProduct from "./components/account/EditProduct";
import ProductList from "./components/products/ProductList";
import ProductDetails from "./components/products/ProductDetails";
import Cart from "./components/products/Cart";
import { ProductProvider } from "./context/product-context";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <App>
      <Routes>
        <Route path="*" element={<Page404></Page404>}></Route>
        <Route path="/" element={<Home />}></Route>
        <Route path="/blog/list" element={<Blog />}></Route>
        <Route path="/blog/detail/:id" element={<BlogDetails />}></Route>
        <Route path="/excu" element={<Excu />}></Route>
        <Route path="/account" element={<Account />}></Route>
        <Route path="/account/add-product" element={<AddProduct />}></Route>
        <Route path="/account/my-product" element={<MyProduct />}></Route>
        <Route
          path="/account/edit-product/:id"
          element={<EditProduct />}
        ></Route>
        <Route path="/products" element={<ProductList />}></Route>
        <Route path="/products/detail/:id" element={<ProductDetails />}></Route>
        <Route path="/cart" element={<Cart></Cart>}></Route>
      </Routes>
    </App>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
