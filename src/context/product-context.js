import axios from "axios";
import { createContext, useContext, useEffect, useState } from "react";

const ProductContext = createContext();
function ProductProvider(props) {
  const [value, setValue] = useState([]);
  const data = { value, setValue };
  return (
    <ProductContext.Provider value={data} {...props}>
      {props.children}
    </ProductContext.Provider>
  );
}
function useProduct() {
  const context = useContext(ProductContext);
  if (typeof context === "undefined") {
    throw new Error("useProduct must be used within ProductProvider");
  }
  return context;
}
export { ProductProvider, useProduct };
